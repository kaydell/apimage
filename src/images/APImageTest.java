package images;

class APImageTest {
    
    public static void main(String[] args) {
        
        // test the zero-arg constructor
        APImage imageFrame = new APImage();
        imageFrame.draw();
        
        // test the saveAs() method
        imageFrame.saveAs();
        
        // test the save() method
        imageFrame.save();

    }
    
}